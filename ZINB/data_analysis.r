source("functions.r")
load("../SIMLR-SIMLR/data/Test_3_Pollen.RData")
load("../SIMLR-SIMLR/data/Test_4_Usoskin.RData")
load("../SIMLR-SIMLR/data/Test_2_Kolod.RData")
load("../SIMLR-SIMLR/data/Test_1_mECS.RData")


library(ggplot2)
library(gridExtra)

name <- c("Buettner" , "Kolodziejczyk", "Pollen", "Usoskin")
p <- list()
Test <- list(Test_1_mECS , Test_2_Kolod, Test_3_Pollen, Test_4_Usoskin)
n_dataset <- length(Test)
MLE <- list()

for (l in 1:n_dataset){
  data<-round(10^(Test[[l]]$in_X)-1.0)
  #sorted <- names(sort(apply(data, 1, var), decreasing = TRUE)) #1000 highly variable genes
  #data0 <- data[sorted[1:1000],]
  data0 <- data
  
  temp <- prior.zinb(data0)
  Theta0 <- temp$Theta0
  var0 <-temp$variance
  
  thet <- MLE.zinb(data0,Theta0,invisible =1)
  
  MLE[[l]]  <- data.frame(mu=thet[,1],theta=thet[,2],pi=thet[,3])
}

#plot graph

for (l in 1:n_dataset){
  xmax <- quantile(MLE[[l]]$mu,.95)
  ymax <- 1
  nn <- length(which(MLE[[l]]$mu < xmax))
  size <- nrow(MLE[[l]])
  
  p[[l]] <- ggplot(MLE[[l]],aes(x=mu,y = pi)) +
    geom_point(shape=18, color="blue") + 
    xlim(c(0,xmax)) +
    ylim(c(0,ymax)) +
    labs(title = name[l],
         x = expression(hat(mu)),
         y = expression(hat(pi)),
         caption = paste(signif(nn/size*100,3),"% shown")) +
    theme(plot.title = element_text(hjust = 0.5))
}

for (l in 1:n_dataset){
  
  #remove genes with low pi
  highpi <- which(MLE[[l]]$pi>0.3)
  xmax <- quantile(MLE[[l]]$mu,.95)
  ymax <- quantile(MLE[[l]]$theta,.95)
  nn <- length(intersect(which(MLE[[l]][highpi,]$mu < xmax),which(MLE[[l]][highpi,]$theta < ymax)))
  #nn <- length(intersect(which(MLE[[l]]$mu < xmax),which(MLE[[l]]$theta < ymax)))
  size <- nrow(MLE[[l]])
  #p[[l]] <- ggplot(MLE[[l]],aes(x=mu,y=theta,colour = pi)) +
  p[[l]] <- ggplot(MLE[[l]][highpi,],aes(x=mu,y=theta,colour = pi)) +
    geom_point() + 
    scale_colour_gradient(low = "blue",high = "yellow", space = "Lab" ,guide = "colourbar",limits=c(0,1)) +
  xlim(c(0,xmax)) +
    ylim(c(0,ymax)) +
    labs(title = name[l],
         x = expression(hat(mu)),
         y = expression(hat(theta)),
         caption = paste(signif(nn/size*100,3),"% shown")) +
    theme(plot.title = element_text(hjust = 0.5))
}

grid.arrange(p[[1]], p[[2]] , p[[3]], p[[4]], nrow = 2)


na <-"MLE_mupi"
png(paste("./Pics/MLE_new/",na,".png",sep = ""),    # create PNG for the heat map        
    width = 3*800,        # 5 x 300 pixels
    height = 3*660,
    res = 300,            # 300 pixels per inch
    pointsize = 8)
grid.arrange(p[[1]], p[[2]] , p[[3]], p[[4]], nrow = 2)
#Heatmap.matrix(Fisher,title = "Aggregate Fisher kernel matrix on mESC dataset", clustering = TRUE)
dev.off()  




Theta <- c(100,1,0.3)
y1 <- rzinb(1000, Theta[2], Theta[1] , Theta[3])
Theta <- c(100,100,0.3)
y2 <- rzinb(1000, Theta[2], Theta[1] , Theta[3])
p1 <-qplot(y1, geom="histogram",
          main = "Histogram for ZINB samples", 
          xlab = "",
          ylab = "frequency") +
  labs(subtitle = paste("dispersion =",1.0 ))
plot(p1)

p2 <-qplot(y2, geom="histogram",
           xlab = "samples",
           ylab = "frequency") +
  labs(subtitle = paste("dispersion =",100.0 ))
plot(p2)
grid.arrange(p1, p2 , nrow = 2)
