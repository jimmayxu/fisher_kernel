library("MASS")
library("ZIM")
source("functions.r")

source("https://bioconductor.org/biocLite.R")
biocLite("scRNAseq")


means <- 10
disp <-2
pi<- 0.4

simu <- rzinb(n = 100, k = disp, lambda = means, omega = pi)

score.zinb(3, means , disp, pi)%*%score.zinb(9, means , disp, pi)
score.zinb(0, means , disp, pi)%*%score.zinb(0, means , disp, pi)

score.zinb(0, means , disp, pi)%*%score.zinb(6, means , disp, pi)




meta <- expression_mRNA_17.Aug.2014[1:9,1:500]

onegene <- as.vector(expression_mRNA_17.Aug.2014[11,])
x <-as.numeric(onegene)
library("scRNAseq")
data(fluidigm)
fluidigm




