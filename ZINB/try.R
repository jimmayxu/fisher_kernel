source("functions.r")
library("ZIM")

# fisher information of negative binomial distribution


y <- rzinb(1000, Theta[2], Theta[1] , Theta[3])
plot(y)
I_estimate(Theta)

  Theta <- c(100,10,0)
  mu <- Theta[1]
  theta <- Theta[2] 
  
  
  y<- rnbinom(1000, size = theta, mu = mu)
  plot(y)
  U  <- array(0,c(2,length(y)))

  U[1,] <- y/mu - (y+theta)/(mu+theta)
  U[2,] <- digamma(y+theta) - digamma(theta) + log(theta/(theta+mu)) + (mu-y)/(theta+mu)
  
  I <- U%*%t(U)/1000
  print(I)
  
  limit <- qnbinom(0.99999,mu = mu, size = theta)
  
  II <- matrix(rep(0,4),ncol=2)
  II[1,1] <- 1/mu - 1/(mu+theta)
  II[2,2] <- sum((1-pnbinom(0:limit, size = theta, mu = mu))/(theta+0:limit)^2)-mu/theta/(theta+mu)
  100*(II - I)/II
  