source("functions.r")
library(gplots)
library(reshape2)
library(gridExtra)
library(ggplot2)

### Simulated samples from ZINB ######
Theta <- c(100,1,0.3)
mu <- Theta[1]
theta <- Theta[2] #aggregation paramter, dispersion parameter
pi <- Theta[3]
heat_x <- c(0,qzinb(0.3,theta,mu,pi):qzinb(0.8,theta,mu,pi))

Fisher_heat <- numeric(0)
Fisher_heat2 <- numeric(0)
Eucliean_heat <- numeric(0)
I <- I_estimate(Theta)
I2 <- I_estimate2(Theta)
for (ll in 1:length(heat_x)){
  Fisher_heat <- cbind(Fisher_heat,Fisher.zinb(cbind(heat_x[ll],heat_x),Theta,I=I))
  Fisher_heat2 <- cbind(Fisher_heat2,Fisher.zinb2(cbind(heat_x[ll],heat_x),Theta,I=I2))
  Eucliean_heat <- cbind(Eucliean_heat,abs(heat_x[ll]-heat_x))
}

Fisher_heat2<-data.matrix(Fisher_heat2)
Eucliean_heat<-data.matrix(Eucliean_heat)
rownames(Fisher_heat) <-  as.character(heat_x); colnames(Fisher_heat) <-  as.character(heat_x)
rownames(Fisher_heat2) <-  as.character(heat_x); colnames(Fisher_heat2) <-  as.character(heat_x)
rownames(Eucliean_heat) <-  as.character(heat_x); colnames(Eucliean_heat) <-  as.character(heat_x)


# Distance
Dist <- list()
l<-1
for (heat in list(Fisher_heat,Fisher_heat2)){
  k <- 1/sqrt(diag(heat))
  G <- heat * (k %*% t(k))
  Dist[[l]] <- sqrt(abs(2 - 2*G))
  l=l+1
}



#use ggplot2 to heatmap

c <- c(1,1,70)
p <- list()
data <- list(Dist[[1]],Dist[[2]] , Eucliean_heat)
name <- c("FZINB kernel distance", "FZINB2 kernel distance", "Euclidean distance")
for (l in 1:3){
  melted <- melt(data[[l]])
  p[[l]] <- ggplot(data =  melted, aes(x=Var1, y=Var2, fill=value)) + 
    geom_tile() +
    scale_fill_gradient2(low = "blue",mid = "white",high = "yellow", space = "Lab" ,guide = "colourbar", midpoint = c[l])+
    labs(title = name[l],
         x = "expression count",
         y = "expression count" ) +
    scale_y_continuous(trans = "reverse")+
    scale_x_continuous(position = "top") +
    theme(plot.title = element_text(hjust = 0.5))+
    theme(aspect.ratio=1)
  plot(p[[l]])
}
png(paste("./Pics/heatmap/FD_theta100.png"),    # create PNG for the heat map        
    width = 6*660,        # 5 x 300 pixels
    height = 3*660,
    res = 300,            # 300 pixels per inch
    pointsize = 8)
grid.arrange(p[[1]], p[[3]],nrow=1)
#grid.arrange(p[[1]],p[[2]], p[[3]])
dev.off()

plot(p[[1]])




### scRNA-seq ######
load(file="../data/Test_1_mECS.RData")
load(file="../data/Test_2_Kolod.RData")
load(file="../data/Test_3_Pollen.RData")
load(file="../data/Test_4_Usoskin.RData")

Test <- list(Test_1_mECS , Test_2_Kolod, Test_3_Pollen, Test_4_Usoskin)
n_dataset <- length(Test)

timeit <-list()
FK <- list()
#compute FZINB kernel
for (n in 1:n_dataset){
  data <- Test[[n]]$in_X
  n_gene <- nrow(data)
  n_cell <- ncol(data)
  X10 <-round(10^data-1.0)
  ptm <- proc.time()
  FK[[n]] <-Fisher.matrix(X10)
  timeit[[n]] <- proc.time() - ptm
}

#compute SIMLR kernel with highest weight
setwd("../SIMLR-SIMLR/")
source("packageimport.r")
D_kernel <- list()
for (n in 1:n_dataset){
  #result <- SIMLR(X=Test[[n]]$in_X,c=Test[[n]]$n_clust)
  #maxK <- max.alphaK(result$alphaK)
  maxK <- 51
  #D_kernel[[n]] <- multiple.kernel.standard(x = t(Test[[n]]$in_X))[[maxK]]
  D_kernel[[n]] <- multiple.kernel(x = t(Test[[n]]$in_X))[[maxK]]
  
}

name <- c("Buettner" , "Kolodziejczyk", "Pollen", "Usoskin")
p <- list()
nmi <- list()
for (n in 1:n_dataset){
  FKD <-Fisher.Distance.matrix(FK[[n]])
  #FKD <- FK[[n]]
  n_clusters <- Test[[n]]$n_clust
  Kmeans_FZINB <- kmeans(FKD, n_clusters, nstart = 30)$cluster
  Kmeans_Gaussian <- kmeans(D_kernel[[n]], n_clusters, nstart = 30)$cluster
  true_clusters <- Test[[n]]$true_labs$V1

  nmi[[n]] <- compare(Kmeans_FZINB,true_clusters,method = "nmi")
  nmi[[n+4]] <- compare(Kmeans_Gaussian,true_clusters,method = "nmi")
  
  clustered <- sort(true_clusters,index.return = TRUE)$ix
  
  melted_FZINB <- melt(FKD[clustered,clustered])
  melted_Gaussian <- melt(as.matrix(D_kernel[[n]])[clustered,clustered])
  pp<-0
  for (melted in list(melted_FZINB , melted_Gaussian )){
    colnames(melted)[3] <- "D"
    limits <- range(melted$D[melted$D!=0])
    
    p[[n+pp]] <- ggplot(data =  melted, aes(x=Var1, y=Var2, fill=D)) + 
      geom_tile() +
      scale_fill_gradient(low = "blue",high = "yellow", space = "Lab" ,guide = "colourbar", limits=limits)+
      labs(y ="",
           caption = paste("NMI ",signif(nmi[[n+pp]],3) ) )+
      #theme_void()+
      theme(axis.line=element_blank(),
            axis.text.x=element_blank(),
            axis.text.y=element_blank(),
            axis.ticks=element_blank(),
            axis.title.x=element_blank(),
            aspect.ratio=1,
            legend.title=element_blank())+
      scale_y_continuous(trans = "reverse")
    
    pp <-4
  }
 
}
  for (n in 1:4){
    p[[n]]<- p[[n]] + theme(plot.title = element_text(hjust = 0.5)) +labs(title = name[n])
  }
  
  p[[1]]<- p[[1]] +  labs(y = "FZINB Kernel Distance")
  p[[5]]<- p[[5]] +  labs(y = "Gaussian Kernel Distance")
  
  plot(p[[5]])

  setwd("../ZINB/")
  png(paste("./Pics/heatmap/distance_compare.png"),    # create PNG for the heat map        
      width = 6*660/2,        # 5 x 300 pixels
      height = 2.3*660/2,
      res = 300/2,            # 300 pixels per inch
      pointsize = 8)
  #grid.arrange(p[[1]], p[[3]],nrow=1)
  grid.arrange(p[[1]],p[[2]], p[[3]],p[[4]],
               p[[5]],p[[6]], p[[7]],p[[8]], nrow=2)
  dev.off()

  
  ### FZINB application ######
  
  
