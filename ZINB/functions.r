library("ZIM")
library("devtools")
library("lbfgs")
##### setting #####

#y <- rzinb(10000, theta, mu , pi)

####################Three parameter (mu, theta, pi) ##########################




##### score ###
"score.zinb" <- function(y , Theta){
  mu <- Theta[1]
  theta <- Theta[2] 
  pi <- Theta[3]
  U  <- array(0,c(3,length(y)))
  frac <- theta/(theta+mu)
  
  U[1,] <- y/mu - (y+theta)/(mu+theta)
  U[2,] <- digamma(y+theta) - digamma(theta) + log(theta/(theta+mu)) + (mu-y)/(theta+mu)
  U[3,] <- -1/(1-pi)

  f0 <- pi + (1-pi)*frac^theta
  U[1,y==0] <- -(1-pi)*frac^(theta+1)/f0
  U[2,y==0] <- (1-pi)*frac^theta*(log(frac)+1-frac)/f0
  U[3,y==0] <- (1-frac^theta)/f0

  return(U)
} 

#### Fisher information matrix ####




"I.zinb" <- function(Theta){
  mu <- Theta[1]
  theta <- Theta[2]
  pi <- Theta[3]
  
  I <- matrix(0,nrow=3,ncol=3)
  frac <- 1/(theta+mu)
  
  fNB0 <- dnbinom(0,mu = mu, size = theta)
  fZINB0 <- dzinb(0, lambda = mu, k = theta, omega = pi)
  score.NB0 <- c(-theta*frac , log(theta*frac)+mu*frac)
  
  Hessian.logNB0 <- matrix(c(theta*frac^2,-mu*frac^2,-mu*frac^2,mu*frac/theta-mu*frac^2), nrow=2)
    
  Nabla.NB0  <- fNB0 * score.NB0
  Hessian.NB0 <-  fNB0* (score.NB0%*%t(score.NB0) + Hessian.logNB0)
  
  PhiPhi.logZINB0 <- (1-pi)*(Hessian.NB0*fZINB0 - (1-pi)*Nabla.NB0%*%t(Nabla.NB0))/fZINB0^2
  Phipi.logZINB0 <- (-Nabla.NB0*fZINB0 - (1-pi)*Nabla.NB0*(1-fNB0))/fZINB0^2
  

  N<-qnbinom(1-10^(-5),mu = mu, size = theta)
  infisum <- sum((1-pnbinom(0:N, mu=mu,size = theta))/(theta+0:N)^2)
  I_NB <- diag(c(1/mu -frac, -mu*frac/theta+infisum))
  

  I[3,3] <- (1 - fNB0)^2/fZINB0 + (1-fZINB0)/(1-pi)^2
  I[1:2,1:2] <- -PhiPhi.logZINB0*fZINB0 + (1-pi)*(I_NB + Hessian.logNB0*fNB0)
  I[1:2,3] <- -Phipi.logZINB0*fZINB0
  I[3,1:2] <- I[1:2,3]

  return(I)
}






"Fisher.zinb" <- function(Y,Theta,iter = 10000, I = NULL){
  if (is.null(I) ) {I <- I.zinb(Theta,iter)}
  L <- t(chol(I))
  if (is.null(dim(Y))){
    Fisher <- t(solve(L,score.zinb(Y[1],Theta)))%*%solve(L,score.zinb(Y[2],Theta))
  }
  else{
    score <- array(0,c(3,dim(Y)[1],2))
    score[,,1] <- score.zinb(Y[,1],Theta)
    score[,,2] <- score.zinb(Y[,2],Theta)
    Fisher <- diag(t(solve(L,score[,,1]))%*%solve(L,score[,,2]))
  }
  return(Fisher)
}


####################Two parameter (mu, theta) ##########################
##### score ###
"score.zinb2" <- function(y , Theta){
  mu <- Theta[1]
  theta <- Theta[2] 
  pi <- Theta[3]
  U  <- array(0,c(2,length(y)))
  frac <- theta/(theta+mu)
  
  U[1,] <- y/mu - (y+theta)/(mu+theta)
  U[2,] <- digamma(y+theta) - digamma(theta) + log(theta/(theta+mu)) + (mu-y)/(theta+mu)

  
  f0 <- pi + (1-pi)*frac^theta
  U[1,y==0] <- -(1-pi)*frac^(theta+1)/f0
  U[2,y==0] <- (1-pi)*frac^theta*(log(frac)+1-frac)/f0

  
  return(U)
} 

"I.zinb2" <- function(Theta){
  return(I.zinb(Theta)[1:2,1:2])
}

"Fisher.zinb2" <- function(Y,Theta,iter = 10000, I = NULL){
  mu <- Theta[1]
  theta <- Theta[2] 
  pi <- Theta[3]
  if (is.null(I) ) {I <- I.zinb2(Theta,iter)}
  L <- t(chol(I))
  if (is.null(dim(Y))){
    Fisher <- t(solve(L,score.zinb2(Y[1],Theta)))%*%solve(L,score.zinb2(Y[2],Theta))
  }
  else{
    score <- array(0,c(2,dim(Y)[1],2))
    score[,,1] <- score.zinb2(Y[,1],Theta)
    score[,,2] <- score.zinb2(Y[,2],Theta)
    Fisher <- diag(t(solve(L,score[,,1]))%*%solve(L,score[,,2]))
  }
  return(Fisher)
}

#### parameter estimation in ZINB #####

"prior.zinb" <- function(X_counts){
  
  mu0 <- apply(X_counts,1, FUN = function(x) mean(x[x!=0]))
  theta0 <- mu0^2/(apply(X_counts, 1, var)-mu0)
  pi0 <- rowSums(X_counts==0)/dim(X_counts)[2]
  Theta0_mean <- c(mean(mu0),mean(theta0),mean(pi0))
  Theta0_var <- c(var(mu0),var(theta0),var(pi0))
  
  result <- list()
  result[["Theta0"]] = Theta0_mean
  result[["variance"]] = Theta0_var
  
  return(result)
}


###### bfgs in parameter mu, theta, pi #######

## INPUT : matrix with genes x cells
## OUTPUT: matrix with genes x 3, each row represents the MLE estimate for that gene


"MLE.zinb" <- function(X,Theta, invisible = 1){
  output <- numeric()
  if (is.null(dim(X))){
    X <- t(matrix(X))
  }
  mu0 <- Theta[1]
  theta0 <- Theta[2] #aggregation paramter, dispersion parameter
  pi0 <- Theta[3]
  phi <- c(log(mu0),log(theta0),qnorm(pi0))
  for (l in 1:dim(X)[1]){
    
    dat <- X[l,]
    
    objective <- function(phi){
      mu <- exp(phi[1])
      theta <- exp(phi[2])
      pi <- pnorm(phi[3])
      n0 <- sum(dat==0)
      datt <- round(dat[dat>0])
      n <- length(dat)
      
      f<- n0 * log(pi + (1-pi)*(theta/(theta+mu))^theta) +
        (n-n0)*log(1-pi) + sum(log(dnbinom(datt,size = theta, mu = mu )))
      return(-f)   
    }
    
    
    gradient <- function(phi){
      mu <- exp(phi[1])
      theta <- exp(phi[2])
      pi <- pnorm(phi[3])
      n0 <- sum(dat==0)  
      datt <- dat[dat>0]
      n <- length(dat)
      
      ratio <- theta/(theta+mu)
      dmu <- -n0*(1-pi)*
        ratio^(theta+1)/(pi + (1-pi)*ratio^theta)+
        sum(datt/mu - (datt+theta)/(mu+theta))
      
      
      dtheta <- n0* (1-pi) *ratio^theta *(log(ratio)+1-ratio)/(pi+(1-pi)*ratio^theta) +  
        sum(digamma(datt+theta) - digamma(theta) + log(ratio) + (mu-datt)/(theta+mu))
      
      dpi <- n0*(1-ratio^theta)/(pi + (1-pi)*ratio^theta)-
        (n-n0)/(1-pi)
      
      dphi1 <- dmu*exp(phi[1])
      dphi2 <- dtheta*exp(phi[2])
      dphi3 <- dpi*dnorm(phi[3])
      
      return(c(-dphi1,-dphi2,-dphi3))
    }
    
    out<- lbfgs(objective, gradient, phi, invisible = invisible)
    
    output <- rbind(output,c(exp(out$par[1]),
                exp(out$par[2]),
                pnorm(out$par[3])))
  }
  colnames(output) <- c("mu","theta","pi")
  return(output)
}


### Fisher matrix ####


"Fisher.matrix" <- function(X_counts, Theta0 = NULL, truncate.ratio = 0.3, n_gene = 1000){
  if (is.null(Theta0)){
    Theta0 <- prior.zinb(X_counts)$Theta0
    if(Theta0[2]<0){
      warning("A Priori parameter estimate of ZINB: Negative size parameter. Reset to 1.")
      Theta0[2] <- 1
    }
  }
  n_cell <- ncol(X_counts)
  THETA <- MLE.zinb(X_counts,Theta0)
  extract <- which(THETA[,3]>truncate.ratio)
  
  extract_sorted <- head(sort(apply(X_counts[extract,], 1, var), decreasing = TRUE,index.return = TRUE)$ix , n_gene)
  sorted <- extract[extract_sorted]
  tempK <- array(0,c(n_cell,n_cell))
  Fisher <- array(0,c(n_cell,n_cell)) #Fisher matrix
  for (l in sorted){
    I <- I.zinb(THETA[l,])
    for (j in 1:n_cell){
      tempK[,j] <- Fisher.zinb(cbind(X_counts[l,],X_counts[l,j]), THETA[l,], I=I)
    }
    Fisher <- Fisher + tempK
  }
  
  return(Fisher/n_gene)
}    


"Fisher.Distance.matrix" <- function(F_matrix){
  k <- 1/sqrt(diag(F_matrix))
  FD_matrix <- sqrt(abs(2 - 2*F_matrix * (k %*% t(k))))
  diag(FD_matrix)<-0
  return(FD_matrix)
} 


################### temporary ####

I_estimate <- function(Theta, iter=10000){
  mu <- Theta[1]
  theta <- Theta[2] 
  pi <- Theta[3]
  x<-rzinb(iter, theta, mu , pi)
  
  U <-  score.zinb(x,Theta) #3 by 1000 matrix
  I <- U%*%t(U)/iter
  return(I)
}

"I.zinb00" <- function(Theta){
  mu <- Theta[1]
  theta <- Theta[2]
  pi <- Theta[3]
  
  "score.NB" <- function(y, mu, theta){
    U  <- array(0,c(2,length(y)))
    
    U[1,] <- y/mu - (y+theta)/(mu+theta)
    U[2,] <- digamma(y+theta) - digamma(theta) + log(theta/(theta+mu)) + (mu-y)/(theta+mu)
    return(U)
  }
  
  "Hessian.logNB"<- function(y, mu,theta ){
    H <- matrix(0,nrow=2,ncol=2)
    H[1,1] <- -y/mu^2  + (y+theta)/(mu+theta)^2
    H[1,2] <- (y-mu)/(mu+theta)^2
    H[2,1] <- H[1,2]
    H[2,2] <- trigamma(y+theta)-trigamma(theta) +1/theta-1/(theta+mu) - (mu-y)/(theta+mu)^2
    return(H)
  }
  
  "Hessian.logZINB" <- function(y, mu, theta, pi){
    H <- matrix(0,nrow=3,ncol=3)
    
    fNB <- dnbinom(y,mu = mu, size = theta)
    fZINB <- dzinb(y, lambda = mu, k = theta, omega = pi)
    
    Nabla.NB  <- fNB * score.NB(y, mu, theta)
    Hessian.NB <-  fNB* (score.NB(y, mu, theta)%*%t(score.NB(y, mu, theta)) + Hessian.logNB(y,mu,theta))
    
    dirac <- as.numeric(y==0)
    H[3,3] <- - (dirac - fNB)^2/fZINB^2
    H[1:2,3] <- (-Nabla.NB*fZINB - (1-pi)*Nabla.NB*(dirac- fNB))/fZINB^2
    H[3,1:2] <- H[1:2,3]
    H[1:2,1:2] <- (1-pi)*(Hessian.NB*fZINB - score.zinb(y,Theta)[1:2]%*%t(Nabla.NB))/fZINB^2
    return(H)
  }
  
  I <- matrix(0,nrow=3,ncol=3)
  y=0
  t<-1
  while (abs(sum(t)) >10^-5){
    t <- Hessian.logZINB(y,mu,theta,pi) * dzinb(y, lambda = mu, k = theta, omega = pi)
    I <- I + t
    y <- y + 1
  }
  print(y)
  return(I)
}


I_11 <- function(Theta){
  Theta[1]<- mu
  Theta[2]<- theta
  Theta[3]<- pi
  f0 <- pi + (1-pi)*(theta/(mu+theta))^theta
  I <- -(1-pi)*(theta+1)*theta^{theta+1}/(mu+theta)^{theta+2} +
    (1-pi)^2*(theta/(mu + theta))^{2*(theta+1)}/f0 +
    (1-pi)/mu -
    ((1-pi)*mu+theta*(1-f0))/(mu+theta)^2
  return(I)
}

"Fisher.matrix.sub" <- function(X_counts, Theta0 = NULL, n_gene = 1000){
  n_cell <- ncol(X_counts) 
  sorted <- head(sort(apply(X_counts, 1, var), decreasing = TRUE,index.return = TRUE)$ix , n_gene) 
  if (is.null(Theta0)){ 
    Theta0 <- prior.zinb(X_counts[sorted,])$Theta0 
    if(Theta0[2]<0){ 
      warning("A Priori parameter estimate of ZINB: Negative size parameter. Reset to 1.") 
      Theta0[2] <- 1 
    } 
  } 
  THETA <- MLE.zinb(X_counts[sorted,],Theta0) 
  rownames(THETA) <- sorted 
  tempK <- array(0,c(n_cell,n_cell)) 
  Fisher <- array(0,c(n_cell,n_cell)) #Fisher matrix 
  for (l in sorted){ 
    I <- I.zinb(THETA[as.character(l),]) 
    for (j in 1:n_cell){ 
      tempK[,j] <- Fisher.zinb(cbind(X_counts[l,],X_counts[l,j]), THETA[as.character(l),], I=I) 
    } 
    Fisher <- Fisher + tempK 
  }
  return(Fisher/n_gene)
}


### estimated Fisher information #####
I_estimate2 <- function(Theta, iter=10000){
  mu <- Theta[1]
  theta <- Theta[2] 
  pi <- Theta[3]
  x<-rzinb(iter, theta, mu , pi)
  I <- array(0,c(2,2))
  
  U <-  score.zinb2(x,Theta) #2 by 1000 matrix
  for (i in 1:iter){
    I <- I + U[,i]%*%t(U[,i])
  }
  return(I/iter)
}