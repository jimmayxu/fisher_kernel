source("functions.r")

library("MASS")
library("ZIM")

n<-50



i <-1
error <- numeric()
while (i < 1000){
  I <-  I_11(Theta)
  diff <-(I_estimate(Theta)[1,1] -I)/I_estimate(Theta)[1,1]
  error<- append(error,diff)
  i<-i+1
}

sum(error^2)/1000

boxplot(error*100,ylab = "Percentage error (%)")
abline(h =quantile(error*100, .975) )


y2 <- c(y,y[1])[-1]
d <- data.frame(y = y, conse = y2, Eucliean = abs(y-y2),Fisher = fisher.zinb(cbind(y,y2),Theta))





#### Graph1 #####
par(mar = c(5,5,2,5))
with(d,plot(y,
            pch=1,
            type="p",
            col = "red3",
            ylab = expression(y[i]),
            xlab=expression(i),
            xaxs="i"))

par(mar = c(5,5,2,5))
with(d,plot(1:n,y[1:n],
            lty=3,
            type="o",
            col = "red3",
            ylab = expression(y[i]),
            xlab=expression(i),
            xlim=c(0,n),xaxs="i"))
par(new = T)
with(d,plot(1:n,(Eucliean/max(Eucliean))[1:n],
            pch = 16,axes= F,
            xlab = NA, ylab = NA, 
            xlim=c(0-0.5,n-0.5),xaxs="i"))

par(new = T)
with(d,plot(1:n,(Fisher)[1:n],
            pch = 17,
            axes = F, 
            xlab = NA, ylab = NA, 
            xlim=c(0-0.5,n-0.5),xaxs="i",col = "blue"))
axis(side = 4)
mtext(side = 4, line = 4, expression( frac(kappa(y[i],y[i+1]), max ) ))

abline(v=1:n, col="gray", lty=3)

legend("top",
       legend=c("counts", "Euclidean distance","Fisher kernel"),
       lty=c(3,0,0), pch=c(1, 16, 17), col=c("red3", "black", "blue"), cex = 0.75)



#### Graph2 #####

plot(y,
    type="p",
    pch=1,
    ylab = expression(y[i]),
    xlab=expression(i),
    xaxs="i")
abline(h=mu,lty=2,col="red")


##### variance of estimated fisher kernel ######

###### boxplot ######
box <- numeric(0)
time <- numeric(0)
for (l in 2:5){
  vec <- numeric(0)
  print(l)
  ptm <- proc.time()
  for (n in 1:1000){
    vec <- append(vec,fisher.zinb(c(mu,0),Theta,iter=10^l))
  }
  time <- append(time,proc.time() - ptm)
  box <- cbind(box,vec)
}
boxplot(box, names=c(100,1000,10000,100000),
        xlab = "iteration in empirical estimation, n", ylab = expression(paste(kappa(mu,0))))



##### value of fisher kernel by varying pi######
#### three parameter #####
g1 <- numeric(0) # Group1 of the type k(0,0)
g2 <- numeric(0) # Group2 of the type k(mu,0)
g3 <- numeric(0) # Group2 of the type k(mu-sd,mu+sd)
pi_set <-seq(.1,.9,.1)

for (pi in pi_set){
  I <- I_estimate(c(mu,theta,pi),iter=10^6)
  g1 <- append(g1,fisher.zinb(c(0,0),c(mu,theta,pi),I=I))
  g2 <- append(g2,fisher.zinb(c(mu,0),c(mu,theta,pi),I=I))
  g3 <- append(g3,fisher.zinb(c(mu-sd,mu+sd),c(mu,theta,pi),I=I))
}

layout(matrix(c(1,1,2,3), 2, 2, byrow = TRUE),
       widths=c(2,2), heights=c(2,2))
plot(cbind(pi_set,g1),xlab = expression(pi),ylab = expression(paste(kappa(0,0))),
     main = expression(paste("Change of Fisher kernel by varying ",pi)))
plot(cbind(pi_set,g2),xlab = expression(pi),ylab = expression(paste(kappa(mu,0))))
plot(cbind(pi_set,g2),xlab = expression(pi),ylab = expression(paste(kappa(mu-sigma,mu+sigma))))


#### two parameter #####
g1 <- numeric(0) # Group1 of the type k(0,0)
g2 <- numeric(0) # Group2 of the type k(mu,0)
g3 <- numeric(0) # Group2 of the type k(mu-sd,mu+sd)
pi_set <-seq(.1,.9,.1)
for (pi in pi_set){
  I <- I_estimate2(c(mu,theta,pi),iter=10^6)
  g1 <- append(g1,fisher.zinb2(c(0,0),c(mu,theta,pi),I=I))
  g2 <- append(g2,fisher.zinb2(c(mu,0),c(mu,theta,pi),I=I))
  g3 <- append(g3,fisher.zinb2(c(mu-sd,mu+sd),c(mu,theta,pi),I=I))
}

layout(matrix(c(1,1,2,3), 2, 2, byrow = TRUE),
       widths=c(2,2), heights=c(2,2))
plot(cbind(pi_set,g1),xlab = expression(pi),ylab = expression(paste(kappa[2](0,0))),
     main = expression(paste("Change of Fisher kernel #2 by varying ",pi)))
plot(cbind(pi_set,g2),xlab = expression(pi),ylab = expression(paste(kappa[2](mu,0))))
plot(cbind(pi_set,g2),xlab = expression(pi),ylab = expression(paste(kappa[2](mu-sigma,mu+sigma))))




###### compare MLE ######

Theta
MLE.zinb(rzinb(10000, 50, 100 , 0.3),c(90,120,0.5))
