source("functions.r")

library(devtools)
library("lbfgs")


objective <- function(phi){
  mu <- exp(phi[1])
  theta <- exp(phi[2])
  pi <- pnorm(phi[3])
  n0 <- sum(dat==0)
  datt <- dat[dat>0]
  n <- length(dat)
  
  f<- n0 * log(pi + (1-pi)*(theta/(theta+mu))^theta) +
    (n-n0)*log(1-pi) + sum(log(dzinb(datt,theta, mu , 0)))
  return(-f)   
}


gradient <- function(phi){
  mu <- exp(phi[1])
  theta <- exp(phi[2])
  pi <- pnorm(phi[3])
  n0 <- sum(dat==0)  
  datt <- dat[dat>0]
  n <- length(dat)
  
  ratio <- theta/(theta+mu)
  dmu <- -n0*(1-pi)*
    ratio^(theta+1)/(pi + (1-pi)*ratio^theta)+
    sum(datt/mu - (datt+theta)/(mu+theta))
  
  
  dtheta <- n0* (1-pi) *ratio^theta *(log(ratio)+1-ratio)/(pi+(1-pi)*ratio^theta) +  
    sum(digamma(datt+theta) - digamma(theta) + log(ratio) + (mu-datt)/(theta+mu))
  
  dpi <- n0*(1-ratio^theta)/(pi + (1-pi)*ratio^theta)-
    (n-n0)/(1-pi)
  
  dphi1 <- dmu*exp(phi[1])
  dphi2 <- dtheta*exp(phi[2])
  dphi3 <- dpi*dnorm(phi[3])
  
  return(c(-dphi1,-dphi2,-dphi3))
  
}



mu0 <- 120
theta0 <- 60 #aggregation paramter, dispersion parameter
pi0 <- 0.6
Theta <- c(mu0,theta0,pi0)


mu <-90 ; theta<-120; pi <-0.3
dat <- rzinb(10000, theta, mu, pi)




### version 3 : bulit-in LBFGS on transformed parameter space ###
phi <- c(log(mu0),log(theta0),qnorm(pi0))

out<- lbfgs(objective, gradient, phi)

output3 <- c(exp(out$par[1]),
      exp(out$par[2]),
      pnorm(out$par[3]))
print(output3)


### version 2 : owned BFGS on transformed parameter space ###

bfgs <- function(gradient,objective, dat , Theta){
  
  B <- diag(3)
  x <- c(log(Theta[1]),log(Theta[2]),qnorm(Theta[3]))
  xx<-matrix(x)
  y<-1
  yy<-array(0,3)
  x0<-array(0,3)
  while (norm(y, type="2") > 0.001 ) {
    x0 <- x
    p <- solve(B,gradient(x0))  
    f <- function (alpha) objective(alpha*p/max(abs(p))+x0) 

    opti <-optimize(f,c(-10,10),tol = 0.0001)
    s <- opti$minimum*p/max(abs(p))
    x <- x0 + s
    y <- gradient(x) - gradient(x0)
    B <- B + (y%*%t(y))/as.numeric(y%*%s) - (B%*%s)%*%(t(s)%*%B)/as.numeric(t(s)%*%(B%*%s))
  }
  return(c(exp(x[1]),exp(x[2]),pnorm(x[3])))
}

output2 <- bfgs(gradient,objective, dat,Theta)


### version 1 : owned BFGS on original parameter space ###

objective <- function(dat,Theta){
  mu <- Theta[1]
  theta <- Theta[2]
  pi <- Theta[3]
  n0 <- sum(dat==0)
  datt <- dat[dat>0]
  n <- length(dat)
  
  f<- n0 * log(pi + (1-pi)*(theta/(theta+mu))^theta) +
    (n-n0)*log(1-pi) + 
    sum(log(dzinb(datt,theta, mu , 0)))
  return(-f)   
}

gradient <- function(dat,Theta){
  mu <- Theta[1]
  theta <- Theta[2]
  pi <- Theta[3]
  n0 <- sum(dat==0)  
  datt <- dat[dat>0]
  n <- length(dat)
  
  ratio <- theta/(theta+mu)
  dmu <- -n0*(1-pi)*
    ratio^(theta+1)/(pi + (1-pi)*ratio^theta)+
    sum(datt/mu - (datt+theta)/(mu+theta))
  
  
  dtheta <- n0* (1-pi) *ratio^theta *(log(ratio)+1-ratio)/(pi+(1-pi)*ratio^theta) +  
    sum(digamma(datt+theta) - digamma(theta) + log(ratio) + (mu-datt)/(theta+mu))
  
  dpi <- n0*(1-ratio^theta)/(pi + (1-pi)*ratio^theta)-
    (n-n0)/(1-pi)
  
  return(c(-dmu,-dtheta,-dpi))
  
}


bfgs0 <- function(gradient,objective, dat , x){
  
  B <- I_estimate(x[1],x[2],x[3])
  xx<-matrix(x)
  x0<-array(0,3)
  while (ncol(xx)<15) {
    x0 <- x
    p <- solve(B,gradient(dat,x0))  
    v <- p/max(abs(p))
    f <- function (alpha) objective(dat,alpha*v+x0) 
    range <- min(abs(1/v[3])-1, min(x0[-3]))
    opti <-optimize(f,c(-range,range),tol = 0.001)
    s <- opti$minimum*p/max(abs(p))
    x <- x0 + s
    y <- gradient(dat,x) - gradient(dat,x0)
    B <- B + (y%*%t(y))/as.numeric(y%*%s) - (B%*%s)%*%(t(s)%*%B)/as.numeric(t(s)%*%(B%*%s))
    xx <- cbind(xx,x)
  }
  return(xx)
}

output0 <- bfgs0(gradient,objective, dat,Theta)

par(mar = c(5,5,2,5))
iter <- ncol(output0)
bound  <- range(output0[-3,],mu,theta)
plot(output0[1,],
     xlab = "no. of iteration (i)",
     ylab = expression(paste(mu," , ",theta)),
     ylim = bound,
     type="b")
points(iter,mu,pch = 16,col = "black")
par(new = T)
plot(output0[2,], axes = F,
     xlab = NA, ylab = NA,
     ylim = bound,
     type = "b",
     col = "red")
points(iter,theta,pch = 16,col = "red")
par(new = T)source("fisher_score.r")

library(devtools)
library("lbfgs")


objective <- function(phi){
  mu <- exp(phi[1])
  theta <- exp(phi[2])
  pi <- pnorm(phi[3])
  n0 <- sum(dat==0)
  datt <- dat[dat>0]
  n <- length(dat)
  
  f<- n0 * log(pi + (1-pi)*(theta/(theta+mu))^theta) +
    (n-n0)*log(1-pi) + sum(log(dzinb(datt,theta, mu , 0)))
  return(-f)   
}


gradient <- function(phi){
  mu <- exp(phi[1])
  theta <- exp(phi[2])
  pi <- pnorm(phi[3])
  n0 <- sum(dat==0)  
  datt <- dat[dat>0]
  n <- length(dat)
  
  ratio <- theta/(theta+mu)
  dmu <- -n0*(1-pi)*
    ratio^(theta+1)/(pi + (1-pi)*ratio^theta)+
    sum(datt/mu - (datt+theta)/(mu+theta))
  
  
  dtheta <- n0* (1-pi) *ratio^theta *(log(ratio)+1-ratio)/(pi+(1-pi)*ratio^theta) +  
    sum(digamma(datt+theta) - digamma(theta) + log(ratio) + (mu-datt)/(theta+mu))
  
  dpi <- n0*(1-ratio^theta)/(pi + (1-pi)*ratio^theta)-
    (n-n0)/(1-pi)
  
  dphi1 <- dmu*exp(phi[1])
  dphi2 <- dtheta*exp(phi[2])
  dphi3 <- dpi*dnorm(phi[3])
  
  return(c(-dphi1,-dphi2,-dphi3))
  
}



mu0 <- 12
theta0 <- 10 #aggregation paramter, dispersion parameter
pi0 <- 0.6
Theta <- c(mu0,theta0,pi0)


mu <-100 ; theta<-90; pi <-0.3
dat <- rzinb(1000, mu, theta , pi)




### version 3 : bulit-in LBFGS on transformed parameter space ###
phi <- c(log(mu0),log(theta0),qnorm(pi0))

out<- lbfgs(objective, gradient, phi)

output3 <- c(exp(out$par[1]),
             exp(out$par[2]),
             pnorm(out$par[3]))



### version 2 : owned BFGS on transformed parameter space ###

bfgs <- function(gradient,objective, dat , Theta){
  
  B <- diag(3)
  x <- c(log(Theta[1]),log(Theta[2]),qnorm(Theta[3]))
  xx<-matrix(x)
  y<-1
  yy<-array(0,3)
  x0<-array(0,3)
  while (norm(y, type="2") > 0.001 ) {
    x0 <- x
    p <- solve(B,gradient(x0))  
    f <- function (alpha) objective(alpha*p/max(abs(p))+x0) 
    
    opti <-optimize(f,c(-10,10),tol = 0.0001)
    s <- opti$minimum*p/max(abs(p))
    x <- x0 + s
    y <- gradient(x) - gradient(x0)
    B <- B + (y%*%t(y))/as.numeric(y%*%s) - (B%*%s)%*%(t(s)%*%B)/as.numeric(t(s)%*%(B%*%s))
  }
  return(c(exp(x[1]),exp(x[2]),pnorm(x[3])))
}

output2 <- bfgs(gradient,objective, dat,Theta)


### version 1 : owned BFGS on original parameter space ###

objective <- function(dat,Theta){
  mu <- Theta[1]
  theta <- Theta[2]
  pi <- Theta[3]
  n0 <- sum(dat==0)
  datt <- dat[dat>0]
  n <- length(dat)
  
  f<- n0 * log(pi + (1-pi)*(theta/(theta+mu))^theta) +
    (n-n0)*log(1-pi) + 
    sum(log(dzinb(datt,theta, mu , 0)))
  return(-f)   
}

gradient <- function(dat,Theta){
  mu <- Theta[1]
  theta <- Theta[2]
  pi <- Theta[3]
  n0 <- sum(dat==0)  
  datt <- dat[dat>0]
  n <- length(dat)
  
  ratio <- theta/(theta+mu)
  dmu <- -n0*(1-pi)*
    ratio^(theta+1)/(pi + (1-pi)*ratio^theta)+
    sum(datt/mu - (datt+theta)/(mu+theta))
  
  
  dtheta <- n0* (1-pi) *ratio^theta *(log(ratio)+1-ratio)/(pi+(1-pi)*ratio^theta) +  
    sum(digamma(datt+theta) - digamma(theta) + log(ratio) + (mu-datt)/(theta+mu))
  
  dpi <- n0*(1-ratio^theta)/(pi + (1-pi)*ratio^theta)-
    (n-n0)/(1-pi)
  
  return(c(-dmu,-dtheta,-dpi))
  
}


bfgs0 <- function(gradient,objective, dat , x){
  
  B <- I_estimate(x[1],x[2],x[3])
  xx<-matrix(x)
  x0<-array(0,3)
  while (ncol(xx)<15) {
    x0 <- x
    p <- solve(B,gradient(dat,x0))  
    v <- p/max(abs(p))
    f <- function (alpha) objective(dat,alpha*v+x0) 
    range <- min(abs(1/v[3])-1, min(x0[-3]))
    opti <-optimize(f,c(-range,range),tol = 0.001)
    s <- opti$minimum*p/max(abs(p))
    x <- x0 + s
    y <- gradient(dat,x) - gradient(dat,x0)
    B <- B + (y%*%t(y))/as.numeric(y%*%s) - (B%*%s)%*%(t(s)%*%B)/as.numeric(t(s)%*%(B%*%s))
    xx <- cbind(xx,x)
  }
  return(xx)
}

output0 <- bfgs0(gradient,objective, dat,Theta)

par(mar = c(5,5,2,5))
iter <- ncol(output0)
bound  <- range(output0[-3,],mu,theta)
plot(output0[1,],
     xlab = "no. of iteration (i)",
     ylab = expression(paste(mu," , ",theta)),
     ylim = bound,
     type="b")
points(iter,mu,pch = 16,col = "black")
par(new = T)
plot(output0[2,], axes = F,
     xlab = NA, ylab = NA,
     ylim = bound,
     type = "b",
     col = "red")
points(iter,theta,pch = 16,col = "red")
par(new = T)
plot(output0[3,], axes = F,
     xlab = NA, ylab = NA,
     type = "b",
     col = "blue")
axis(side = 4)
mtext(side = 4, line = 4, expression( pi ))
points(iter,pi,pch = 16,col = "blue")

legend("top",
       legend=c(expression(mu[i]),expression(theta[i]),expression(pi[i]),"true value of parameter"),
       lty=c(1,1,1,0), pch=c(1, 1, 1,16), col=c("black", "red","blue"), cex = 0.75)
output0 <- output0[,ncol(output0)]

plot(output0[3,], axes = F,
     xlab = NA, ylab = NA,
     type = "b",
     col = "blue")
axis(side = 4)
mtext(side = 4, line = 4, expression( pi ))
points(iter,pi,pch = 16,col = "blue")

legend("top",
       legend=c(expression(mu[i]),expression(theta[i]),expression(pi[i]),"true value of parameter"),
       lty=c(1,1,1,0), pch=c(1, 1, 1,16), col=c("black", "red","blue"), cex = 0.75)
output0 <- output0[,ncol(output0)]
