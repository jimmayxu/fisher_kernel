library(Matrix)
library(parallel)

# load the igraph package to compute the NMI
library(igraph)

# load the palettes for the plots
library(grDevices)

# load the kernel k-mean method
library(kernlab)
# load sample form dirichlet distribution
library(MCMCpack)

# load the SIMLR R package
source("./R/SIMLR.R")
source("./R/compute.multiple.kernel.R")
source("./R/network.diffusion.R")
source("./R/utils.simlr.R")
source("./R/tsne.R")

source("../ZINB/functions.r")

source("./R/my_SIMLR.R")
source("my_kernel.r")
source("./R/my_SIMLR_kmean.r")
source("supp_function.r")
library(gplots)
library(reshape2)
library(gridExtra)
library(ggpubr)

#dyn.load("./R/projsplx_R_linux.so")
dyn.load("./R/projsplx_R_mac.so")
which <- Matrix::which