#install.packages("RcppAnnoy")
#setRepositories()
#2
#install.packages("SIMLR")
# required external packages for SIMLR
setwd("~/Bitbucket/fisher_kernel/SIMLR-SIMLR")


source("packageimport.r")

#dyn.load("./R/projsplx_R_win.so")

# load the daKernelstasets
load(file="../data/Test_1_mECS.RData")
load(file="../data/Test_2_Kolod.RData")
load(file="../data/Test_3_Pollen.RData")
load(file="../data/Test_4_Usoskin.RData")
load(file="../data/Zelsel.RData")

Test <- list(Test_1_mECS , Test_2_Kolod, Test_3_Pollen, Test_4_Usoskin)
n_dataset <- length(Test)
iteration <- 1
result <- list()
result_F <- list()
time <-as.numeric()
for (n in 1:n_dataset){
  #sorted <- names(sort(apply(datalist[[n]], 1, var), decreasing = TRUE))
  #data <- datalist[[n]][sorted[1:1000],]
  data <- Test[[n]]$in_X
  n_gene <- nrow(data)
  n_cell <- ncol(data)
  X10 <-round(10^data-1.0)
  Theta0 <- prior.zinb(X10)$Theta0
  THETA <- MLE.zinb(X10,Theta0)
  extract <- which(THETA[,3]>0.3)
  for (iter in 1:iteration){
    #result[iter,n] <- tryCatch(compare(labellist[[n]],SIMLR(X=data,c=clist[[n]])$y$cluster,method="nmi"),error = function(e) return(0))
    result[[iteration*(n-1)+iter]] <-SIMLR(X=data[extract,],c=Test[[n]]$n_clust)
    ptm <- proc.time()
    result_F[[iteration*(n-1)+iter]] <-SIMLR.Fisher(X=data[extract,],c=Test[[n]]$n_clust)
    time <- append(time,proc.time() - ptm)
    #result[[iteration*(n-1)+iter]] <-tryCatch(SIMLR.Fisher(X=data,c=clist[[n]]),error = function(e) return(0))
  }
}


nmi <-as.numeric()
for (l in 1:length(result)){
  nmi <- append(nmi,compare(Test[[l]]$true_labs[,1],result_F[[l]]$y$cluster,method="nmi"))
}
compare(Test_2_Kolod$true_labs[,1],result[[2]]$y$cluster,method="nmi")
compare(Test_3_Pollen$true_labs[,1],result[[3]]$y$cluster,method="nmi")

# a Priori estimation of ZINB

for (n in 1:n_dataset){
  sorted <- names(sort(apply(datalist[[n]], 1, var), decreasing = TRUE))
  data <- datalist[[n]][sorted[1:1000],]
  xx<-prior.zinb(data^10-1)
  print(xx$Theta0)
}
  
# Plot the weights
## RBF kernel with Euclidean distance + Fisher Kernel + RBF kernel with Fisher distance
png(paste("./pics/weights/Buettner.png"),    # create PNG for the heat map        
    width = 5*300,        # 5 x 300 pixels
    height = 3*300,
    res = 200,            # 300 pixels per inch
    pointsize = 8)
#plot(res_example.test$alphaK, ylab = "Weights", xlab = "index of Kernels", col= c(rep(1,55),rep(2,55),4), main = "SIMLR")
#legend("right", legend=c("Gaussian kernel with Euclidean distance","Gaussian kernel with Fisher distance", "aggregate Fisher Kernel"),
#       col=c(1,2,4), cex=0.8, pch = rep(1,3))
p
dev.off() 


K<-data.frame("index"= 1:55,"w"=res_example1$alphaK , "sigma" = as.character(rep(seq(1,2,0.25),11)), "k" = as.character(rep(seq(10,30,2),each = 5)))
p <- ggplot(K, aes(x=index, y=w,  shape = sigma, color = k)) + geom_point() + labs(title = "Weights of kernel")
p

  # test SIMLR.R on example 1
  set.seed(11111)
  res_example1 = SIMLR(X=Test_1_mECS$in_X,c=Test_1_mECS$n_clust)
  res_example11 = SIMLR.FZINB(X=Test_1_mECS$in_X,c=Test_1_mECS$n_clust,IncludeGaussian = FALSE)
  res = SIMLR.COMBINE(X=Test_1_mECS$in_X,c=Test_1_mECS$n_clust)
  
  # apply SIMLR on truncated data with first 1000 most variable genes
  sorted <- names(sort(apply(Test_1_mECS$in_X, 1, var), decreasing = TRUE))
  data <- Test_1_mECS$in_X[sorted[1:1000],]
  res_example111 = SIMLR.Fisher(X=data,c=Test_1_mECS$n_clust)
  compare(Test_1_mECS$true_labs[,1],res_example11$y$cluster,method="nmi")
  
  # test SIMLR.R on example 3
 set.seed(33333)
 cat("Performing analysis for Test_3_Pollen","\n")
 res_example3 = SIMLR(X=Test_3_Pollen$in_X,c=Test_3_Pollen$n_clust)
 res_example31 = SIMLR.Fisher(X=Test_3_Pollen$in_X,c=Test_3_Pollen$n_clust)
 compare(Test_3_Pollen$true_labs[,1],res_example31$y$cluster,method="nmi")
 
 
 res_example3k = SIMLR.kkmean(X=Test_3_Pollen$in_X,c=Test_3_Pollen$n_clust)
  # test SIMLR.R on example 4
 set.seed(444442)
  res_example4 = SIMLR(X=Test_4_Usoskin$in_X,c=Test_4_Usoskin$n_clust)
  res_example41 = SIMLR.Fisher(X=Test_4_Usoskin$in_X,c=Test_4_Usoskin$n_clust)
  compare(Test_4_Usoskin$true_labs[,1],res_example41$y$cluster,method="nmi")
  
  # test SIMLR.R on example 2
  set.seed(22222)
  cat("Performing analysis for Test_2_Kolod","\n")
  res_example2 = SIMLR(X=Test_2_Kolod$in_X,c=Test_2_Kolod$n_clust)
  res_example21 = SIMLR.Fisher(X=Test_2_Kolod$in_X,c=Test_2_Kolod$n_clust)
  compare(Test_2_Kolod$true_labs[,1],res_example21$y$cluster,method="nmi")
  

  # kernel k means
  S <-res_example3$S
  c <- Test_3_Pollen$n_clust
  y <- kkmeans(as.kernelMatrix(S),centers = c, kernel=matrix)
  compare(Test_3_Pollen$true_labs[,1],y@.Data,method="nmi")
  
  
# make the scatterd plots
  par(mfrow=c(1,3)) 
  name <- c("55 RBF (Euclidean) ", "4 RBF + 1 Fisher (mESC)" , "55 RBF (Fisher distance)")
plot(res_example1$ydata,col=c(topo.colors(Test_1_mECS$n_clust))[Test_1_mECS$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main= name[1])
plot(res_example11$ydata,col=c(topo.colors(Test_1_mECS$n_clust))[Test_1_mECS$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main=name[2])
plot(res_example12$ydata,col=c(topo.colors(Test_1_mECS$n_clust))[Test_1_mECS$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main=name[3])

plot(res_example2$ydata,col=c(topo.colors(Test_2_Kolod$n_clust))[Test_2_Kolod$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main= name[1])
plot(res_example21$ydata,col=c(topo.colors(Test_2_Kolod$n_clust))[Test_2_Kolod$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main=name[2])
plot(res_example22$ydata,col=c(topo.colors(Test_2_Kolod$n_clust))[Test_2_Kolod$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main=name[3])


plot(res_example3$ydata,col=c(topo.colors(Test_3_Pollen$n_clust))[Test_3_Pollen$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main= name[1])
plot(res_example31$ydata,col=c(topo.colors(Test_3_Pollen$n_clust))[Test_3_Pollen$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main=name[2])
plot(res_example32$ydata,col=c(topo.colors(Test_3_Pollen$n_clust))[Test_3_Pollen$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main=name[3])

par(mfrow=c(1,1)) 
plot(res_example4$ydata,col=c(topo.colors(Test_4_Usoskin$n_clust))[Test_4_Usoskin$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main="SIMILR 2D visualization for Test_4_Usoskin")
plot(res_example42$ydata,col=c(topo.colors(Test_4_Usoskin$n_clust))[Test_4_Usoskin$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main="SIMILR 2D visualization for Test_4_Usoskin")



###  heatmap of similarity matrix
library("ggplot2")

method <- "- RBF(Fisher)"

#method <- "- RBF(Euclidean)"
names <- list("mESC","Kolod","Pollen","Usoskin")
result <- list(res_example12$S,res_example22$S,res_example32$S,res_example42$S)
dat <- list(Test_1_mECS$true_labs[,1], Test_2_Kolod$true_labs[,1], Test_3_Pollen$true_labs[,1] , Test_4_Usoskin$true_labs[,1])
for (l in 1:4){
  name <-names[[l]]
  true_lab <- sort(dat[[l]],index.return=1)$ix
  DD <- result[[l]][true_lab,true_lab]
  
  png(paste("./pics/heatmap/",name,"1.png"),    # create PNG for the heat map        
      width = 5*300,        # 5 x 300 pixels
      height = 5*300,
      res = 300,            # 300 pixels per inch
      pointsize = 8)
  n <- dim(DD)[1]
  x <- data.frame(row=rep(1:n,each=n),col=rep(n:1,times=n), S=as.vector(DD))
  HM <- ggplot(x, aes(x=row, y=col, fill = S)) + geom_tile()
  HM <- HM + scale_fill_gradient2(low='blue', mid ='white', high ='yellow', midpoint=mean(range(DD)), guide = "colourbar")
  HM <- HM  + scale_x_discrete(expand = c(0, 0))
  HM <- HM  + scale_y_discrete(expand = c(0, 0))
  HM <- HM  + coord_equal()
  HM <- HM  + labs(title = paste(name,method)) 
  HM <- HM  + theme(plot.title = element_text(hjust = 0.5), 
                    axis.text=element_blank(), 
                    axis.ticks=element_blank(),
                    axis.title=element_blank(),
                    plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))
  print(HM)
  dev.off()   
}


## external data set




## truncated for Kolod

test <-Test_1_mECS
X <- test$in_X
n_gene <- nrow(X)
n_cell <- ncol(X)
X10 <-round(10^X-1.0)
Theta0 <- prior.zinb(X10)$Theta0
THETA <- MLE.zinb(X10,Theta0)
extract <- which(THETA[,3]>0.3)
XX <- X10[extract,]
ptm <- proc.time()
res_example.test = SIMLR.Fisher(X=X[extract,],c=test$n_clust)
time <- append(time,proc.time() - ptm)
res_example = SIMLR(X=X[extract,],c=test$n_clust)



compare(test$true_labs[,1],res_example.test$y$cluster,method="nmi")
plot(res_example.test$ydata,col=c(topo.colors(Test_4_Usoskin$n_clust))[Test_4_Usoskin$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main="SIMILR 2D visualization for Test_4_Usoskin")



## truncated for Usoskin

X <- Test_4_Usoskin$in_X
n_gene <- nrow(X)
n_cell <- ncol(X)
X10 <-10^X-1.0
THETA <- MLE.zinb(X10,c(10000,.1,0.5))
extract <- intersect(which(THETA[,3]>0.7), which(THETA[,2]>1))

XX <- X[extract,]
res_example.test = SIMLR2(X=XX,c=Test_4_Usoskin$n_clust)
res_example = SIMLR(X=XX,c=Test_4_Usoskin$n_clust)

compare(Test_4_Usoskin$true_labs[,1],res_example$y$cluster,method="nmi")
plot(res_example.test$ydata,col=c(topo.colors(Test_4_Usoskin$n_clust))[Test_4_Usoskin$true_labs[,1]],xlab="SIMLR component 1", ylab="SIMLR component 2",pch=20,main="SIMILR 2D visualization for Test_4_Usoskin")
