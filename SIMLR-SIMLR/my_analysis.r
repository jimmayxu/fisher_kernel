source("packageimport.r")
load(file="../data/Test_1_mECS.RData")
load(file="../data/Test_2_Kolod.RData")
load(file="../data/Test_3_Pollen.RData")
load(file="../data/Test_4_Usoskin.RData")

Test <- list(Test_1_mECS , Test_2_Kolod, Test_3_Pollen, Test_4_Usoskin)
n_dataset <- length(Test)


result_SIMLR <- list()
result_FZINB <- list()
result_three <- list()

for (truncated in c(FALSE,TRUE)){
  for (n in 1:n_dataset){
    if (truncated){
      X <- Test[[n]]$in_X
      X10 <-round(10^X-1.0)
      Theta0 <- prior.zinb(X10)$Theta0
      THETA <- MLE.zinb(X10,Theta0)
      extract <- which(THETA[,3]>0.5)
      testdata <- X[extract,]
    }
    else{
      testdata <- Test[[n]]$in_X
    }
    result_SIMLR[[n+truncated*4]] <- SIMLR(X=testdata,c=Test[[n]]$n_clust)
    result_FZINB[[n+truncated*4]] <- SIMLR.FZINB(X=testdata,c=Test[[n]]$n_clust,IncludeGaussian = FALSE)
    result_three[[n+truncated*4]] <- SIMLR.FZINB(X=testdata,c=Test[[n]]$n_clust,IncludeGaussian = TRUE)
  }
}



#### Heatmap for similarity matrix ###
p <- list()
name <- c("Buettner" , "Kolodziejczyk", "Pollen", "Usoskin")

nmi <-list()

for (n in 1:n_dataset){
  true_clusters <- Test[[n]]$true_labs$V1
  clustered <- sort(true_clusters,index.return = TRUE)$ix

  pp<-0
  for (result in list(result_SIMLR[[n]],result_FZINB[[n]],result_three[[n]])){
    S <- result$S
    melted <- melt(S[clustered,clustered])
    
    limits <- as.vector(quantile(result$S,c(0,.95)))
    nmi <- compare(true_clusters,result$y$cluster, method = "nmi")
    
    p[[n+pp]] <- ggplot(data =  melted, aes(x=Var1, y=Var2, fill=value)) + 
      geom_tile() +
      scale_fill_gradient(low = "blue",high = "yellow", space = "Lab" ,guide = "colourbar", limits=limits)+
      labs(y ="",
           caption = paste("NMI ",signif(nmi,3) ) )+
      #theme_void()+
      theme(axis.line=element_blank(),
            axis.text.x=element_blank(),
            axis.text.y=element_blank(),
            axis.ticks=element_blank(),
            axis.title.x=element_blank(),
            aspect.ratio=1,
            legend.title=element_blank())+
      scale_y_continuous(trans = "reverse")
    
    pp <-pp+n_dataset
  }
}
  

for (n in 1:n_dataset){
  p[[n]]<- p[[n]] + theme(plot.title = element_text(hjust = 0.5)) +labs(title = name[n])
}
#name <- c("FZINB_variants","SIMLR","Threekind")
p[[1]]<- p[[1]] +  labs(y ="Gaussian based on Euclidean distance (SIMLR)")
p[[5]]<- p[[5]] +  labs(y =  "Gaussian based on FZINB distance")
p[[9]]<- p[[9]] +  labs(y = "three kinds of kernels")

plot(p[[1]])

png(paste("./pics/S/similarity_compare.png"),    # create PNG for the heat map        
    width = 2*6.5*660/2/2,        # 5 x 300 pixels
    height = 2*4*660/2/2,
    res = 400/2/2,            # 300 pixels per inch
    pointsize = 8)
#grid.arrange(p[[1]], p[[3]],nrow=1)
  grid.arrange(p[[1]],p[[2]], p[[3]],p[[4]],
             p[[5]],p[[6]], p[[7]],p[[8]],
             p[[9]],p[[10]], p[[11]],p[[12]],nrow=3)
dev.off()


#plot weights
p_w <-list()
for (n in 1:n_dataset){
  K<-data.frame("index"= 1:110,"weights"=result_three[[n]]$alphaK ,"candidates" = c(rep("Gaussian kernel in Euclidean distance",55),rep("Gaussian kernel with FZINB distance",55)))
  p_w[[n]] <- ggplot(K, aes(x=index, y=weights,shape = candidates,  color = candidates)) + geom_point() + 
    labs(title = name[[n]]) +
    theme(legend.position = "bottom",
          legend.title=element_blank(),
          plot.title = element_text(hjust = 0.5),
          axis.text.y=element_blank())+
    scale_shape_discrete(breaks=c("Gaussian kernel in Euclidean distance","Gaussian kernel with FZINB distance"))+
    scale_color_discrete(breaks=c("Gaussian kernel in Euclidean distance","Gaussian kernel with FZINB distance"))
}
png(paste("./pics/weights/threekind.png"),    # create PNG for the heat map        
    width = 8*660,        # 5 x 300 pixels
    height = 3*660,
    res = 500,            # 300 pixels per inch
    pointsize = 8)
ggarrange(p_w[[1]],p_w[[2]], p_w[[3]],p_w[[4]],nrow=1,ncol=4,  common.legend = TRUE, legend="bottom")
dev.off()
