library(Matrix)
library(parallel)
library(igraph)
# load the palettes for the plots
library(grDevices)
library(ggplot2)
# load the SIMLR R package
source("./R/SIMLR.R")
source("./R/compute.multiple.kernel.R")
source("./R/network.diffusion.R")
source("./R/utils.simlr.R")
source("./R/tsne.R")

source("../ZINB/functions.r")

source("./R/my_SIMLR.R")
source("my_kernel.r")
source("supp_function.r")
dyn.load("./R/projsplx_R.so")

# Load the data from 10X Genomics


datacounts <- I0X_Genomics.load("../data/pbmc8k",title = title,download = FALSE)
datalog <- data.matrix(log(datacounts+1,10))

data <- datacounts

which <- Matrix::which





# Load the data from Zelsel


load(file="../data/Zelsel.RData")
load(file="../data/Test_4_Usoskin.RData")
load(file="../data/Test_3_Pollen.RData")
datalog <- Zelsel$in_X
datacounts <- 10^datalog-1

data <- datacounts

### heatmap ###
dataform <- "(counts)"
dataform <- "(logcounts)"
title <- "mESC"
dataname <- "mESC" 

#sorted <- names(sort(rowSums(data), decreasing = TRUE))
sorted <- names(sort(apply(data, 1, var), decreasing = TRUE)) #1000 highly variable genes
data0 <- data[sorted[1:1000],]

plot(datalog[sorted[102],] , main = paste("102th highly variable gene in",title),ylab = "log10(count+1)", xlab = "Cells")
plot(datacounts[sorted[102],] , main = paste("102th highly variable gene in",title),ylab = "barcode counts", xlab = "Cells")

# preview

p <- Heatmap.matrix(data0,title,paste(dataform,"matrix of 1000 hightly expressed genes"))
p <- p + labs(x = "Genes", y = "Cells")
png(paste("./pics/data_demo/",dataname,"2.png",sep = ""),    # create PNG for the heat map        
    width = 3*660,        # 5 x 300 pixels
    height = 3*460,
    res = 300,            # 300 pixels per inch
    pointsize = 8)
plot(p)
dev.off()
##


apriori <- prior.zinb(data0)
Theta0 <- apriori$Theta0
var0 <- apriori$variance
print(Theta0)
Theta0[2] <-1
thet <- MLE.zinb(data0,Theta0,invisible =1)


MLE <- data.frame(mu=thet[,1],theta=thet[,2],pi=thet[,3])
ymax <- 3
xmax <- 5000

xmax <- Theta0[1]+sqrt(var0[1])
ymax <- Theta0[2]+sqrt(var0[2])
nn <- length(intersect(which(MLE$mu < xmax),which(MLE$theta < ymax)))

p <- ggplot(MLE,aes(x=mu,y=theta)) +
  geom_point(aes(colour = pi)) + scale_colour_gradient(low = "blue", 
                                                       high = "yellow", space = "Lab" ,guide = "colourbar") +
  xlim(c(0,xmax)) +
  ylim(c(0,ymax)) +
  labs(title = paste("ZINB MLE",dataform),
       #labs(title = "ZINB MLE on (exp(logcount) - 1)",     
       subtitle=paste("fitted by 1000 most highly variable genes in",title),
       x = expression(hat(mu)),
       y = expression(hat(theta)),
       caption = paste(nn,"/1000 shown"))
plot(p)

png(paste("./pics/MLE/",dataname,"3.png",sep = ""),    # create PNG for the heat map        
    width = 3*660,        # 5 x 300 pixels
    height = 3*460,
    res = 300,            # 300 pixels per inch
    pointsize = 8)
plot(p)
dev.off()  






X <- Zelsel$in_X
X_counts <- round(10^X-1.0)

X_counts <- X_counts[,seq(1,ncol(X_counts),6)]
X_counts <- X_counts[-which(rowSums(X_counts)==0),]
prior.zinb(X_counts)

