setwd("~/YEAR1/fisher_kernel/SIMLR-SIMLR/")
source("packageimport.r")
load(file="../data/Test_1_mECS.RData")
load(file="../data/Test_2_Kolod.RData")
load(file="../data/Test_3_Pollen.RData")
load(file="../data/Test_4_Usoskin.RData")

Test <- list(Test_1_mECS , Test_2_Kolod, Test_3_Pollen, Test_4_Usoskin)
n_dataset <- length(Test)


result_SIMLR <- list()
result_FZINB <- list()
result_three <- list()

for (truncated in c(FALSE,TRUE)){
  for (n in 1:n_dataset){
    if (truncated){
      X <- Test[[n]]$in_X
      X10 <-round(10^X-1.0)
      Theta0 <- prior.zinb(X10)$Theta0
      THETA <- MLE.zinb(X10,Theta0)
      extract <- which(THETA[,3]>0.5)
      testdata <- X[extract,]
    }
    else{
      testdata <- Test[[n]]$in_X
    }
    result_SIMLR[[n+truncated*4]] <- SIMLR(X=testdata,c=Test[[n]]$n_clust)
    result_FZINB[[n+truncated*4]] <- SIMLR.FZINB(X=testdata,c=Test[[n]]$n_clust,IncludeGaussian = FALSE)
    result_three[[n+truncated*4]] <- SIMLR.FZINB(X=testdata,c=Test[[n]]$n_clust,IncludeGaussian = TRUE)
  }
}


remove(Test)
remove(Test_1_mECS)
remove(Test_2_Kolod)
remove(Test_3_Pollen)
remove(Test_4_Usoskin)
remove(testdata)
remove(X)
remove(X10)
save.image("../hpc/results/heatmap.RData")


