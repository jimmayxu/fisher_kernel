#!/bin/bash -i

#SBATCH -J heatmap
#SBATCH --mem=32gb                     # Job memory request
#SBATCH --cpus-per-task=8            # Number of CPU cores per task
#SBATCH --time=12:00:00               # Time limit hrs:min:sec
#SBATCH --output=serial_test_%j.log   # Standard output and error log

#SBATCH --mail-type=ALL


module load R
#
echo "I ran on:"
cd $SLURM_SUBMIT_DIR
echo $SLURM_NODELIST
#
srun R CMD BATCH ./task1.r


